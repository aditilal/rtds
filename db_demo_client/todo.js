//selecting dom elements for manipulation
var input = document.querySelector("input[type = 'text']");
var ul = document.querySelector("ul");
var container = document.querySelector("div");
var lists = document.querySelectorAll("li");
var spans = document.getElementsByTagName("span");
var pencil = document.querySelector("#pencil");
var saveBtn = document.querySelector(".save");
var clearBtn = document.querySelector(".clear");
var tipsBtn = document.querySelector(".tipBtn");
var closeBtn = document.querySelector(".closeBtn");
var overlay = document.getElementById("overlay")

// App logic to handle the DB data
function addToDo(message, key) {
	var li = document.createElement("li");
	var spanElement = document.createElement("span");
	var icon = document.createElement("i");
	
	var newTodo = message[key].title;
	this.value = " " ;

	// Strike-out the data
	if (message[key].checked) {
		li.classList.toggle('checked');
	}

	icon.classList.add('fas', 'fa-trash-alt');
	spanElement.append(icon);
	ul.appendChild(li).append(spanElement,newTodo);
}


//function to delete todo if delete span is clicked.
function deleteTodo(){
  for(let span of spans){
    span.addEventListener ("click",function (){
      span.parentElement.remove();
      event.stopPropagation();
    });
  }
}

//event listener for input to add new todo to the list.
input.addEventListener("keypress",function(keyPressed){
  if(keyPressed.which === 13){
    //creating lists and span when enter is clicked
	var newTodo = this.value;
	this.value = " " ;
    client.publish(db_name, {query: 'post', key: 'title', data:{'title':newTodo.trim(), checked: false}});
  }
    
});


// event listener to linethrough list if clicked
ul.addEventListener('click', function(ev) {
	let title = ev.target.innerText;
	if (ev.target.className === 'checked' ) {
		client.publish(db_name, {query: 'post', key: 'title', data:{'title':title, checked: false}});
	}
	else {
		client.publish(db_name, {query: 'post', key: 'title', data:{'title':title, checked: true}});
	}
	
    ev.target.classList.toggle('checked');
  },false
);
