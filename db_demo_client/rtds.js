var client = new Faye.Client('http://localhost:5454/rtds');
var data = {};
var db_name = '/test_db2';
// Util function for object size
Object.size = function(obj) {
    var size = 0, key;
    for (key in obj) {
        if (obj.hasOwnProperty(key)) size++;
    }
    return size;
};

client.addExtension({
    outgoing: function(message, callback) {
        if (message.channel !== '/meta/subscribe')
        return callback(message);
        message.ext = message.ext || {};
        message.ext.auth = "db_token";
        callback(message);
    }
});



// subscribe to db table
var subscription = client.subscribe(db_name, function(message) {
    // incoming changes - This call dumps db data 
    console.log(JSON.stringify(message));
    if (Object.size(message) > 0) {
        ul.innerHTML= "";
        for (let key in message) {
            if ('title' in message[key]) {
                addToDo(message, key);
            }
        }
    }
});

// publish changes to db
/*
{query:'get'} - gets data from subscribed database table
{query:'post', key: ..., data: {...}} - modifies/adds data from subscribed database table
{query:'delete', key: '...', data: {}} - deletes object from subscribed database table
*/
subscription.then(function() {
    client.publish(db_name, {query: 'get'});
});