var http = require('http'),
    faye = require('faye');
var fs = require('fs');
var server = http.createServer(),
    bayeux = new faye.NodeAdapter({ mount: '/rtds', timeout: 45 });

var server = http.createServer(function(request, response) {
    response.writeHead(200, { 'Content-Type': 'text/plain' });
    response.end('Welcome to RTDS');
});

var serverAuth = {
    incoming: function(message, callback) {
        // Get subscribed channel and auth token
        var subscription = message.subscription;
        var msgToken = message.ext  && message.ext.auth;

        // Find the right db auth/token for the channel
        this._fileContent = this._fileContent || fs.readFileSync('./config/db_config.json');

        var registry = JSON.parse(this._fileContent.toString());
        //var token = registry["test_db"];

        if (message.channel !== '/meta/subscribe') {
            if ((message.channel).substring(1) == 'explore') {
                
                let db_index = JSON.parse(fs.readFileSync('./persistence/collections/collection.json'));
                message.data = db_index;
                
                return callback(message);
            }
            else if ((message.channel).substring(1) in registry) {
                let data = message.data;
                if ('query' in data) {
                    // Accesss the persistence
                    let db_index = JSON.parse(fs.readFileSync('./persistence/collections/collection.json'));
                    var persistence_data = {};

                    let db_channel = (message.channel).substring(1);

                    if (db_index['db'].includes(db_channel)) {
                        persistence_data = JSON.parse(fs.readFileSync('./persistence/collections/'+db_channel+'.json'));
                    }
                    else {
                        // db data not found, create a new one
                        var json = JSON.stringify({}); //convert it back to json
                        fs.writeFile('./persistence/collections/'+db_channel+'.json', json, 'utf8',callback);
                        db_index['db'].push(db_channel);
                        fs.writeFile('./persistence/collections/collection.json', JSON.stringify(db_index), 'utf8',callback);
                    }

                    if (data.query === 'get') {
                        message.data = persistence_data;
                        message.from = 'server';
                    }
                    else if (data.query === 'post') {
                        let key = data.key.trim();
                        persistence_data[data.data[key]] = data.data;
                        message.data = persistence_data;
                        message.from = 'server';
                        json = JSON.stringify(persistence_data); //convert it back to json
                        fs.writeFile('./persistence/collections/'+db_channel+'.json', json, 'utf8', callback);

                    }
                    else if (data.query === 'delete') {
                        let key = data.key.trim();
                        delete persistence_data[data.data[key]];
                        message.data = persistence_data;
                        message.from = 'server';
                        json = JSON.stringify(persistence_data); //convert it back to json
                        fs.writeFile('./persistence/collections/'+db_channel+'.json', json, 'utf8', callback);
                    }
                }
                else {
                    message.error = 'Invalid DB operation';
                }
            }

            return callback(message);
        }
        else {
            /*
            let persistence_data = JSON.parse(fs.readFileSync('./persistence/collections/'+db_channel+'.json'));
            message.data = persistence_data;
            console.log("in sub "+ JSON.stringify(message));*/
            callback(message);
        }

        // Add an error if the tokens don't match
        /*
        if (token !== msgToken)
            message.error = 'Invalid DB auth';*/

        // Call the server back now we're done
        callback(message);
    }
};

bayeux.addExtension(serverAuth);
bayeux.attach(server);
server.listen(5454);