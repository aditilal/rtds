# RTDS


## Installing
Clone the repository and

> cd rtds

> npm install

## Running the DB server
> node index.js

The DB server will run on port 5454

As a security measure, ```db_config.json``` holds the allowed db collections and tokens. Any client/applications can only read and write to collections authorized in this config. 
The client has to connect with RTDS with the db name and corrosponding token to establish the web-socket connection.

## Running the DB UI
DB UI gives easy access to the data in the RTDS for each collection and updates in real-time as client read and write new data.

> cd db_ui

And open the html file in the browser. The JS written in the HTML automatically connects to DB server through web-socket.

## Running the Demo DB Client application
I have added a simple To-do list application. This is to demonstrate how any kind of applications use RTDS using JS. 
```rtds.js``` file outlines how the app handles real-time data from RTDS and renders it.

> cd db_demo_client

And open the html file in the browser.